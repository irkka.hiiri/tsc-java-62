package ru.tsc.ichaplygina.taskmanager.api.service;

import javax.jms.MessageListener;

public interface IReceiverService {

    void receive(MessageListener listener);

}
