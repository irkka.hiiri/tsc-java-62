package ru.tsc.ichaplygina.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.dto.UserDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserRecordRepository extends AbstractRecordRepository<UserDTO> {

    void deleteAll();

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findFirstByEmail(@NotNull String email);

    @NotNull
    Optional<UserDTO> findById(@NotNull String id);

    @Nullable
    UserDTO findFirstByLogin(@NotNull String login);

    @Nullable
    @Query("select id from UserDTO e where e.email = :email")
    String findFirstIdByEmail(@NotNull @Param("email") String email);

    @Nullable
    @Query("select id from UserDTO e where e.login = :login")
    String findFirstIdByLogin(@NotNull @Param("login") String login);

    long count();

    void deleteById(@NotNull String id);

    void deleteByLogin(@NotNull String login);

}
