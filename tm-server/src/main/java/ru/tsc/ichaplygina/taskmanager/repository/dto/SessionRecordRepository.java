package ru.tsc.ichaplygina.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionRecordRepository extends AbstractRecordRepository<SessionDTO> {

    long count();

    void deleteAll();

    void deleteById(@NotNull final String id);

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    Optional<SessionDTO> findById(@NotNull final String id);

}
