package ru.tsc.ichaplygina.taskmanager.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.ITaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.User;
import ru.tsc.ichaplygina.taskmanager.repository.model.TaskRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskService extends AbstractBusinessEntityService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final Task task) {
        repository.saveAndFlush(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final User user = Optional.ofNullable(userService.findById(userId)).orElseThrow(UserNotFoundException::new);
        @NotNull final Task task = new Task(name, description, user);
        add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<Task> taskList) {
        if (taskList == null) return;
        for (final Task task : taskList) add(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public final Task addTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @Nullable final Task task = userService.isPrivilegedUser(userId) ?
                repository.findById(taskId).orElse(null) :
                repository.findFirstByUserIdAndId(userId, taskId);
        if (task == null) return null;
        @Nullable final Project project = Optional.ofNullable(projectService.findById(userId, projectId)).orElseThrow(ProjectNotFoundException::new);
        task.setProject(project);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(final String userId) {
        if (userService.isPrivilegedUser(userId)) repository.deleteAll();
        else repository.deleteByUserId(userId);
    }

    @Nullable
    @Override
    @Transactional
    public Task completeById(@NotNull final String userId, @Nullable final String taskId) {
        return updateStatus(userId, taskId, Status.COMPLETED);
    }

    @Nullable
    @Override
    @Transactional
    public Task completeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, repository.count())) throw new IndexIncorrectException(index);
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByPage(PageRequest.of(index, 1)).stream().findFirst().orElse(null) :
                repository.getIdByPageForUser(userId, PageRequest.of(index, 1)).stream().findFirst().orElse(null);
        return completeById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public Task completeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByName(name) :
                repository.getIdByNameForUser(userId, name);
        if (id == null) return null;
        return completeById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId) {
        return userService.isPrivilegedUser(userId) ?
                repository.findAll() : repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        @NotNull final Comparator<Task> comparator = getComparator(sortBy);
        return userService.isPrivilegedUser(userId) ?
                repository.findAll().stream().sorted(comparator).collect(Collectors.toList()) :
                repository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public final List<Task> findAllByProjectId(@NotNull final String userId,
                                               @NotNull final String projectId,
                                               @Nullable final String sortBy) {
        @NotNull final Comparator<Task> comparator = getComparator(sortBy);
        return userService.isPrivilegedUser(userId) ?
                repository.findByProjectId(projectId).stream().sorted(comparator).collect(Collectors.toList()) :
                repository.findByUserIdAndProjectId(userId, projectId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String userId, @Nullable final String taskId) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        return userService.isPrivilegedUser(userId) ?
                repository.findById(taskId).orElse(null) :
                repository.findFirstByUserIdAndId(userId, taskId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        return userService.isPrivilegedUser(userId) ?
                repository.findByIndex(PageRequest.of(entityIndex, 1)).stream().findFirst().orElse(null) :
                repository.findByIndexForUser(userId, PageRequest.of(entityIndex, 1)).stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        return userService.isPrivilegedUser(userId) ?
                repository.findFirstByName(entityName) :
                repository.findFirstByUserIdAndName(userId, entityName);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int index) {
        return userService.isPrivilegedUser(userId) ?
                repository.getIdByPage(PageRequest.of(index, 1)).stream().findFirst().orElse(null) :
                repository.getIdByPageForUser(userId, PageRequest.of(index, 1)).stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        return userService.isPrivilegedUser(userId) ?
                repository.getIdByName(entityName) : repository.getIdByNameForUser(userId, entityName);
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.count();
    }

    @Override
    @SneakyThrows
    public long getSize(@NotNull final String userId) {
        return userService.isPrivilegedUser(userId) ?
                repository.count() : repository.countByUserId(userId);
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        return getSize(userId) == 0;
    }

    @Override
    @SneakyThrows
    @Transactional
    public final void removeAllByProjectId(@NotNull final String projectId) {
        repository.deleteByProjectId(projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @Nullable final Task task = findById(id);
        if (task == null) return null;
        repository.deleteById(id);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeById(@NotNull final String userId, @NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.deleteById(id);
        else repository.deleteByUserIdAndId(userId, id);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @Nullable Task task = findByIndex(userId, index);
        if (task == null) return null;
        if (userService.isPrivilegedUser(userId) || task.getUser().getId().equals(userId)) repository.delete(task);
        else task = null;
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.deleteByName(name);
        else repository.deleteByUserIdAndName(userId, name);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public final Task removeTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @Nullable final Task task = userService.isPrivilegedUser(userId) ?
                repository.findFirstByIdAndProjectId(taskId, projectId) :
                repository.findFirstByUserIdAndIdAndProjectId(userId, taskId, projectId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setProject(null);
        repository.saveAndFlush(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task startById(@NotNull final String userId, @Nullable final String taskId) {
        return updateStatus(userId, taskId, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    @Transactional
    public Task startByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, repository.count())) throw new IndexIncorrectException(index);
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByPage(PageRequest.of(index, 1)).stream().findFirst().orElse(null) :
                repository.getIdByPageForUser(userId, PageRequest.of(index, 1)).stream().findFirst().orElse(null);
        return startById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public Task startByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByName(name) :
                repository.getIdByNameForUser(userId, name);
        return startById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task updateById(@NotNull final String userId,
                           @NotNull final String taskId,
                           @NotNull final String taskName,
                           @Nullable final String taskDescription) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        if (isEmptyString(taskName)) throw new NameEmptyException();
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setName(taskName);
        task.setDescription(taskDescription);
        repository.saveAndFlush(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task updateByIndex(@NotNull final String userId,
                              final int entityIndex,
                              @NotNull final String entityName,
                              @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    @Transactional
    private Task updateStatus(@NotNull final String userId, @Nullable final String taskId, @NotNull final Status status) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setStatus(status);
        repository.saveAndFlush(task);
        return task;
    }

}
