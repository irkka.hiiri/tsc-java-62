package ru.tsc.ichaplygina.taskmanager.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;

public interface IUserService extends IAbstractService<User> {

    void add(@NotNull String login, @NotNull String password, @NotNull String email, @NotNull Role role,
             @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

    void add(@NotNull User user);

    void clear();

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable User findByLoginForAuthorization(@NotNull String login);

    @Nullable
    String findIdByLogin(@NotNull String login);

    boolean isPrivilegedUser(@NotNull String userId);

    boolean lockById(@NotNull String id);

    boolean lockByLogin(@NotNull String login);

    @Nullable
    User removeByLogin(@NotNull String login);

    void setPassword(@NotNull String login, @NotNull String password);

    void setRole(@NotNull String login, @NotNull Role role);

    boolean unlockById(@NotNull String id);

    boolean unlockByLogin(@NotNull String login);

    @Nullable
    User updateById(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull String email,
                    @NotNull Role role, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

    @Nullable
    User updateByLogin(@NotNull String login, @NotNull String password, @NotNull String email,
                       @NotNull Role role, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

}
