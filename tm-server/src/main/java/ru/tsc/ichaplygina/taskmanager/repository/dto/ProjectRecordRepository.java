package ru.tsc.ichaplygina.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectRecordRepository extends AbstractBusinessEntityRecordRepository<ProjectDTO> {

    long count();

    long countByUserId(@NotNull String userId);

    void deleteAll();

    void deleteById(@NotNull String id);

    void deleteByName(@NotNull String name);

    void deleteByUserId(@NotNull String userId);

    @NotNull
    @Query("SELECT e FROM ProjectDTO e")
    List<ProjectDTO> findByIndex(@NotNull Pageable pageable);

    @NotNull
    @Query("SELECT e FROM ProjectDTO e where e.userId = :userId")
    List<ProjectDTO> findByIndexForUser(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<ProjectDTO> findAll();

    @Nullable
    @Query("SELECT id FROM ProjectDTO")
    List<String> getIdByPage(@NotNull Pageable pageable);

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull String userId);

    @NotNull
    Optional<ProjectDTO> findById(@NotNull String id);

    @Nullable
    ProjectDTO findFirstByIdAndUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    ProjectDTO findFirstByName(@NotNull String name);

    @Nullable
    ProjectDTO findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    ProjectDTO findFirstByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Nullable
    @Query("SELECT id FROM ProjectDTO e WHERE e.name = :name")
    String getIdByName(@NotNull @Param("name") String name);

    @Nullable
    @Query("SELECT id FROM ProjectDTO e WHERE e.userId = :userId AND e.name = :name")
    String getIdByNameForUser(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @Query("SELECT id FROM ProjectDTO e where e.userId = :userId")
    List<String> getIdByPageForUser(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

}
