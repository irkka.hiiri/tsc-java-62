package ru.tsc.ichaplygina.taskmanager.comparator;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;

import java.util.Comparator;

public class CommandComparator implements Comparator<Class<? extends AbstractListener>> {

    @NotNull
    private static final CommandComparator INSTANCE = new CommandComparator();

    private CommandComparator() {
    }

    @NotNull
    public static CommandComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(Class o1, Class o2) {
        if (o1.getSuperclass().getName().equals(o2.getSuperclass().getName()))
            return o1.getName().compareTo(o2.getName());
        return o1.getSuperclass().getName().compareTo(o2.getSuperclass().getName());
    }

}
