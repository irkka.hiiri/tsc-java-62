package ru.tsc.ichaplygina.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class TaskCompleteByNameListener extends AbstractTaskListener {

    @NotNull
    public final static String DESCRIPTION = "complete task by name";
    @NotNull
    public final static String NAME = "complete task by name";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskCompleteByNameListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String name = readLine(NAME_INPUT);
        getTaskEndpoint().completeTaskByName(sessionService.getSession(), name);
    }

}
