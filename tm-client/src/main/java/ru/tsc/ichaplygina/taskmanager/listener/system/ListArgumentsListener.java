package ru.tsc.ichaplygina.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;

import java.util.List;

@Component
public final class ListArgumentsListener extends AbstractListener {

    @NotNull
    public final static String CMD_NAME = "list commands";

    @NotNull
    public final static String DESCRIPTION = "list available commands";

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @Nullable
    @Override
    public final String argument() {
        return null;
    }

    @NotNull
    @Override
    public final String command() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@listArgumentsListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println();
        for (@NotNull final AbstractListener listener : listeners) {
            System.out.println(listener.command());
        }
        System.out.println();
    }

}
