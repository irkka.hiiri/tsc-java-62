package ru.tsc.ichaplygina.taskmanager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.ichaplygina.taskmanager.endpoint.*;

@Configuration
@ComponentScan("ru.tsc.ichaplygina.taskmanager")
public class ClientConfiguration {

    @Bean
    @NotNull
    public AdminEndpoint adminEndpoint() {
        @NotNull final AdminEndpointService adminEndpointService = new AdminEndpointService();
        return adminEndpointService.getAdminEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint() {
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint() {
        @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
        return taskEndpointService.getTaskEndpointPort();
    }

}
